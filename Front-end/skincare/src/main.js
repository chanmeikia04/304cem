//Firebase
import firebase from "firebase/app";
import "firebase/storage";

//vue
import Vue from 'vue'
import App from './App.vue'
import router from './router'

//Bootstrap
import {BootstrapVue, BootstrapVueIcons} from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'


Vue.use(BootstrapVue)
Vue.use(BootstrapVueIcons)
Vue.component('Navbar',require('./components/tool/NavBar.vue').default);
Vue.config.productionTip = false

router.beforeEach((to, from, next) => {
  // console.log('上一个页面：', from)
  // console.log('下一个页面：', to)
 
  let userToken = localStorage.getItem('username')
  let role = localStorage.getItem('role')
 
  if (to.meta.requireAuth) {
    if (userToken) {
      if (to.meta.Admin) {
        console.log('role：', role)
        if(role == 'Admin'){
          next()
        }else {
          next({
            path: '/'
          })
        }
      }else {
        next()
      }
    } else {
      next({
        path: '/signin'
      })
    }
  } else {
    next()
  }
 
  if (to.fullPath === '/signin') {
    if (userToken) {
      next({
        path: from.fullPath
      })
    } else {
      next()
    }
  }
})

new Vue({
  router,
  render: h => h(App),

  created() {
    this.$router.push('/')

    //Firebase configuration
    var firebaseConfig = {
      apiKey: "AIzaSyDP9KQUbioPwB4lmj94oFmCpk6C6A1Wpzo",
      authDomain: "cem-8a775.firebaseapp.com",
      projectId: "cem-8a775",
      storageBucket: "cem-8a775.appspot.com",
      messagingSenderId: "580332152740",
      appId: "1:580332152740:web:ff0b9537ceeb8964f6c331",
      measurementId: "G-KXHCVHC1GZ"
    };

    // Initialize Firebase
    firebase.initializeApp(firebaseConfig);
    firebase.analytics();
  }
}).$mount('#app')
