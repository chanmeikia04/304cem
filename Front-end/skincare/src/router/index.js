import Vue from 'vue'
import Router from 'vue-router'
import Homepage from '@/components/Homepage'
import ProductDetail from '@/components/ProductDetail'
import Signin from '@/components/LoginAndRegister/Signin'
import Signout from '@/components/LoginAndRegister/Signout'
import Register from '@/components/LoginAndRegister/Register'
import SkinCareList from '@/components/Admin/SkinCare/SkinCareList'
import CreateSkinCare from '@/components/Admin/SkinCare/CreateSkinCare'
import ModifySkinCare from '@/components/Admin/SkinCare/ModifySkinCare'
import CommentsList from '@/components/Admin/Comments/CommentsList'
import MyCommentsList from '@/components/MyComments'
import MyCommentModify from '@/components/MyComment_Modify'

Vue.use(Router)

export default new Router({
    routes: [
      {
        path: '/',
        name: 'Homepage',
        component: Homepage,
      },
      {
        path: '/productDetail',
        name: 'ProductDetail', 
        component: ProductDetail,
        meta: {requireAuth: true}             
      },
      {
        path: '/signin',
        name: 'Signin', 
        component: Signin
      },
      {
        path: '/signout',
        name: 'Signout', 
        component: Signout,
        meta: {requireAuth: true}  
      },
      {
        path: '/register',
        name: 'Register', 
        component: Register
      },
      {
        path: '/skinCareList',
        name: 'SkinCareList', 
        component: SkinCareList,
        meta: {requireAuth: true, Admin: true}  
      },
      {
        path: '/createSkinCare',
        name: 'CreateSkinCare', 
        component: CreateSkinCare,
        meta: {requireAuth: true, Admin: true}  
      },
      {
        path: '/modifySkinCare',
        name: 'ModifySkinCare', 
        component: ModifySkinCare,
        meta: {requireAuth: true, Admin: true}  
      },
      {
        path: '/commentsList',
        name: 'CommentsList', 
        component: CommentsList,
        meta: {requireAuth: true, Admin: true}  
      },
      {
        path: '/myCommentsList',
        name: 'MyCommentsList', 
        component: MyCommentsList,
        meta: {requireAuth: true}  
      },
      {
        path: '/myCommentModify',
        name: 'MyCommentModify', 
        component: MyCommentModify,
        meta: {requireAuth: true}  
      },
      
    ]
})