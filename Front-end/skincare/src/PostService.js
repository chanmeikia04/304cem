import axios from 'axios'   //plus-in

const url = "http://localhost:3000/posts"   //Back-end url

class PostService{  //call --> components --> HelloWorld.vue
    static getPosts(){      //get Data
        return new Promise((resolve, reject) => {
            axios.get(url).then((res) => {
                const data = res.data

                resolve(
                    data.map(post =>({
                        ...post,
                        date: new Date(post.date)
                    }))
                )
            }).catch((err) => {
                reject(err)
            })
        })
    }
}

export default PostService