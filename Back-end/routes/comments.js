const express = require('express');
const router = express.Router();
const Comments = require('../models/Comments');

//All the commends
router.get('/', async(req, res) => {
    try{
        const comment = await Comments.find();
        res.json(comment);
    }catch(err){
        res.json({message: err});
    }
});

// All the commends of commendID
router.get('/:postId', async (req,res) => {
    try {
         const comment = await Comments.find({"_id": req.params.postId});
         res.json(comment);
    }catch (err) {
        res.json({message: err});
    }    
});

// All the commends of product
router.get('/productID/:postId', async (req,res) => {
    try {
         const comment = await Comments.find({"productID": req.params.postId});
         res.json(comment);
    }catch (err) {
        res.json({message: err});
    }    
});

// All the commends of users
router.get('/userID/:postId', async (req,res) => {
    try {
         const comment = await Comments.find({"userID": req.params.postId});
         res.json(comment);
    }catch (err) {
        res.json({message: err});
    }    
});

//Create
router.post('/',async (req,res) => {
    const commend = new Comments({
        userID: req.body.userID,
        productID: req.body.productID,
        comments: req.body.comments,
        rank: req.body.rank
    });
    try{   
        const savedCommend = await commend.save()
        res.json(savedCommend)
    }catch (err) {
       res.json({message: err}); 
    }
});

//Update
router.patch('/:postId', async (req,res) => {
    try{
    const updatedComment = await Comments.updateOne(
        {_id: req.params.postId}, 
        {$set: {comments: req.body.comments, rank: req.body.rank}}
     );
    res.json(updatedComment);    
    }catch (err) {
    res.json({message: err});
    }
})

//Delete
router.delete('/:postId', async (req,res) => {
    try {
    const removedCommend = await Comments.remove({_id: req.params.postId}) 
    res.json(removedCommend);
    }catch (err) {
    res.json({message: err});
    }
});

module.exports = router;