const express = require('express');
const router = express.Router();
const config = require('config');
const jwt = require ('jsonwebtoken');
const {check, validationResult} = require('express-validator');
const Users = require('../models/Users');
const bcrypt = require('bcryptjs');
const auth = require('../middlewares/auth');

/** private route, Logged In users Can access it */
router.get('/', auth, async (req, res)   => {
   try {
       const user = await (await Users.findById(req.user.id)).Selected('-password');
       res.json(user);
   } catch (err) {
       console.log(err.message);
       res.status(500).send('error');
   }

});

router.post('/', [
    check('username', 'Please enter valid username').not().isEmpty(),
    check('password', 'Please enter password').exists()
],
    async (req, res) => {
    const errors = validationResult(req);
    if( !errors.isEmpty){ return res.status(400).json({errors: errors.array()})}

    const {username, password}  = req.body;
    var isAdmin = false;
    try {
        let user = await Users.findOne({username});
        if(!user){return res.status(400).json({msg: 'Incorrect username.'});} 

        const checkpassword = await bcrypt.compare(password, user.password);
        if (!checkpassword) { return res.status(400).json({ msg: 'Incorrect Password'});}

        isAdmin = user.isAdmin;
        // const payload = { user: { id: user.id}};
    } catch (error) {
       console.log(error.message);
       res.status(500).send('Server Error');
    }    
    res.json({ msg: 'Login success', isAdmin: isAdmin, username: username});
});

module.exports = router;