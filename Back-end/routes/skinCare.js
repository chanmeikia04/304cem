const express = require('express');
const router = express.Router();
const SkinCare = require('../models/SkinCare');

//get all the skin care info
router.get('/', async(req, res) => {
    try{
        const skinCare = await SkinCare.find();
        res.json(skinCare);
    }catch(err){
        res.status(400).json({message: err});
    }
});

//get all the skin care info by type
router.get('/:postId', async(req, res) => {
    try{
        const skinCare = await SkinCare.find({"type": req.params.postId});
        res.json(skinCare);
    }catch(err){
        res.status(400).json({message: err});
    }
});

//get all the skin care info by id
router.get('/id/:postId', async(req, res) => {
    try{
        const skinCare = await SkinCare.find({"_id": req.params.postId});
        res.json(skinCare);
    }catch(err){
        res.status(400).json({message: err});
    }
});

//Create
router.post('/',async (req,res) => {
    const skinCare = new SkinCare({
        productPhoto: req.body.productPhoto,
        productName: req.body.productName,
        type: req.body.type,
        price: req.body.price,
        description: req.body.description
    });
    try{   
        const savedSkinCare = await skinCare.save()
        res.json(savedSkinCare)
    }catch (err) {
       res.status(400).json({message: err}); 
    }
});

//Update
router.patch('/:postId', async (req,res) => {
    try{
    const updatedSkinCare = await SkinCare.updateOne(
        {_id: req.params.postId}, 
        {$set: {productName: req.body.productName, type: req.body.type, price: req.body.price, description: req.body.description}}
     );
    res.json(updatedSkinCare);    
    }catch (err) {
    res.status(400).json({message: err});
    }
})

//Delete
router.delete('/:postId', async (req,res) => {
    try {
    const removedSkinCare = await SkinCare.remove({_id: req.params.postId}) 
    res.json(removedSkinCare);
    }catch (err) {
    res.status(400).json({message: err});
    }
});

module.exports = router;