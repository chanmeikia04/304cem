const express = require('express');
const router = express.Router();
const config = require('config');
const jwt = require('jsonwebtoken');
const { check, validationResult } = require('express-validator');
const bcrypt = require('bcryptjs');

const Users = require('../models/Users');

/**   registeration user route */
router.post('/', [
    check('username', 'Please enter username with more than or equal to 5 characters.').isLength({ min: 5 })
    
], async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty) { return res.status(400).json({ errors: errors.array() }) }

    const { username, isAdmin, password } = req.body;
    try {
        /** User is exists  */
        let user = await Users.findOne({ username });
        if (user) { return res.status(400).json({ msg: 'User is already Exists.' }); }
        user = new Users({ username, isAdmin, password })

        
        /** password with Hash */
        const salt = await bcrypt.genSalt(10);
        user.password = await bcrypt.hash(password, salt);
        await user.save();
        const payload = {
            user: {
                id: user.id
            }
        };
    } catch (error) {
    }

    res.json({ msg: 'Register Success' });
});

module.exports = router;