const mongoose = require('mongoose');

const SkinCareSchema = mongoose.Schema({
    productPhoto:{
        type: String,
        require: true
    },
    productName:{
        type: String,
        require: true
    },
    type:{
        type:String,
        require: true
    },
    price:{
        type:String,
        require: true
    },
    description:{
        type:String,
        require: true
    },
    date: { 
        type: Date,
        default: Date.now
    }   
});

module.exports = mongoose.model('SkinCare', SkinCareSchema);