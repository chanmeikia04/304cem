const mongoose = require('mongoose');

const CommendSchema = mongoose.Schema({
    userID:{
        type: String,
        require: true
    },
    productID:{
        type: String,
        require: true
    },
    comments:{
        type: String,
        require: true
    },
    rank:{
        type:String,
        require: true
    },
    date: { 
        type: Date,
        default: Date.now
    }   
});

module.exports = mongoose.model('Commends',CommendSchema );