const express = require('express');
const app = express();
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cors = require('cors');

require('dotenv/config');


app.use(bodyParser.json());
app.use(cors());


//Import Routes
const skinCareRoute = require('./routes/skinCare');
const usersRoute = require('./routes/users');
const commendRoute = require('./routes/comments');
const authRoute = require('./routes/auth');

app.use('/skinCares', skinCareRoute);
app.use('/users', usersRoute);
app.use('/comments', commendRoute);
app.use('/auth', authRoute);


//Routes
app.get('/', (req,res) => {res.send('testing server');});


//Connect to DB
mongoose.connect( process.env.DB_CONNECTION, { useNewUrlParser: true },  () => console.log('connected to DB!'));


//start Listening
app.listen(3000);